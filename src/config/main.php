<?php

return [
    'namespaces' => [
        'controller' => '', // Default: <AppNamespace>\\Http\\Controllers
        'model' => '', // Default: <AppNamespace>\\Models
    ],

    'contentParamsName' => 'contentParams',

    'copyTo' => [
        'config' => config_path('laravel_bootstrap_modal.php'),
        'view' => resource_path('views/shared/modals'),
    ],
];