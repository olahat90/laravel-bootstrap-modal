@section('content')
    @parent

    <div class="modal fade" id="MyEditorModal" data-backdrop="static" style="z-index: 1047;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Bezár</span></button>
                    <h4 class="modal-title">&nbsp;</h4>
                </div>
                <div class="modal-body">
                    <div class="callout callout-danger modal-form-error hidden"></div>

                    <form class="form-horizontal" role="form" autocomplete="off" method="post"></form>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Mégsem</button>
                        <button type="button" class="btn btn-primary btn-raised btn-flat" data-url=""
                                data-redirect-url="">
                            <span class="fa fa-floppy-o"></span>&nbsp; Mentés
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@push('afterScripts')
    <script type="text/javascript">
        function editorModalInit() {
            $('[data-editor]').unbind('click').on('click', function (e) {
                $this = $(this);
                $row = $this.parents('tr');

                var MODAL = $('#MyEditorModal');

                var data = ($row.length && $this.parents('table').hasClass('dataTable')) ? $this.parents('table').DataTable().row($row).data() : new Object();
                e.preventDefault();

                $.ajax({
                    type: "GET",
                    data: data,
                    url: $this.data('editorModalContentUrl'),
                    beforeSend: function () {
                        MODAL.find('.modal-title').html($this.data('editorModalTitle'));
                        MODAL.find('.modal-body').find('form').html('Kérem várjon ...');
                        //MODAL.modal('toggle');
                    },
                    success: function (result) {
                        MODAL.find('button.btn-primary').data('url', $this.data('editorSubmitUrl'));
                        MODAL.find('.modal-body').find('form').html(result);
                        if ($this.data('editorId') == 0) {
                            MODAL.find('.modal-body').find('form :input.form-control').first().focus();
                        }
                    },
                    error: function (error) {
                    }
                });
            });
        }

        $(function () {
            var MODAL = $('#MyEditorModal');

            $('#MyEditorModal').on('hidden.bs.modal', function (e) {
                if (e.target.id == 'MyEditorModal') {
                    MODAL.find('.modal-body').find('form').children().remove();
                    MODAL.find('.modal-body').find('form').removeAttr('enctype');
                }
            });

            $('#MyEditorModal button.btn-primary').on('click', function () {
                $this = $(this);

                if (MODAL.find('form').attr('enctype')) {
                    MODAL.find('form').attr('action', MODAL.find('button.btn-primary').data('url'));
                    MODAL.find('form').submit();
                } else {
                    MODAL.find('textarea').each(function () {
                        if (typeof(CKEDITOR) != 'undefined' && CKEDITOR.instances[$(this).attr('name')]) {
                            $(this).val(CKEDITOR.instances[$(this).attr('name')].getData());
                        }
                    });
                    var data = MODAL.find('form').serialize();

                    $.ajax({
                        type: "POST",
                        cache: false,
                        data: data,
                        url: $this.data('url'),
                        beforeSend: function () {
                            MODAL.find('.modal-error-div').addClass('hidden').html('');
                        },
                        success: function (result) {
                            if (result.trim()) {

                                if (result.substring(0, 5) == 'eval;') {
                                    eval(result.substring(5));
                                } else {
                                    MODAL.find('.modal-body').find('form').html(result);
                                    MODAL.animate({
                                        scrollTop: MODAL.offset().top
                                    }, 2000);
                                }
                            } else {
                                if ($this.data('redirectUrl')) {
                                    document.location = $this.data('redirectUrl');
                                } else {
                                    document.location.reload(true);
                                }
                            }
                        },
                        error: function (error) {
                            if (error.status == 422) {
                                // Validation error
                                var errorsHtml = '';

                                $.each(error.responseJSON, function (key, value) {
                                    errorsHtml += '<p>' + value[0] + '</p>';
                                });
                                errorsHtml += '';

                                MODAL.find('.modal-form-error').removeClass('hidden').html(errorsHtml);
                            } else if (error.status == 500) {
                                // Internal Server Error
                                MODAL.find('.modal-form-error').removeClass('hidden').html(error.responseJSON.error);
                            }
                        }
                    });
                }
            });

            editorModalInit();
        });
    </script>
@endpush