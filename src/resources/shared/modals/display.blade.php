@section('content')
    @parent

    <div class="modal fade" id="MyDisplayModal" data-backdrop="static" style="z-index: 1045;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Bezár</span></button>
                    <h4 class="modal-title">&nbsp;</h4>
                </div>
                <div class="modal-body">
                    <div class="callout callout-danger modal-form-error hidden"></div>

                    <form class="form-horizontal" role="form" autocomplete="off" method="post"></form>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Bezár</button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@push('afterScripts')
    <script type="text/javascript">
        function displayModalInit() {
            $('[data-display]').unbind('click').on('click', function (e) {
                $this = $(this);
                $row = $this.parents('tr');

                var MODAL = $('#MyDisplayModal');

                var data = ($row.length && $this.parents('table').hasClass('dataTable')) ? $this.parents('table').DataTable().row($row).data() : new Object();
                e.preventDefault();

                $.ajax({
                    type: "GET",
                    data: data,
                    url: $this.data('displayModalContentUrl'),
                    beforeSend: function () {
                        MODAL.find('.modal-title').html($this.data('displayModalTitle'));
                        MODAL.find('.modal-body form').html('Kérem várjon ...');
                        //MODAL.modal('toggle');
                    },
                    success: function (result) {
                        MODAL.find('.modal-body form').html(result);
                    },
                    error: function (error) {
                        MODAL.find('.modal-body .modal-form-error').html('Hiba történt a betöltés közben!');
                    }
                });
            });
        }

        $(function () {
            var MODAL = $('#MyDisplayModal');

            MODAL.on('hidden.bs.modal', function (e) {
                MODAL.find('.modal-body form').children().remove();
            });

            displayModalInit();
        });
    </script>
@endpush