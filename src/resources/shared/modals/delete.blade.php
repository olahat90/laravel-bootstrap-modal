@section('content')
    @parent

    <div class="modal fade" id="MyDeleteModal" data-backdrop="static" style="z-index: 1046;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Bezár</span></button>
                    <h4 class="modal-title">&nbsp;</h4>
                </div>
                <div class="modal-body">
                    <div class="callout callout-danger modal-form-error hidden"></div>

                    <form class="form-horizontal" role="form" autocomplete="off"></form>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Mégsem</button>
                        <button type="button" class="btn btn-danger btn-raised btn-flat" data-url=""
                                data-redirect-url=""><span class="fa fa-trash-o"></span>&nbsp; Törlés
                        </button>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@push('afterScripts')
    <script type="text/javascript">
        function deleteModalInit() {
            $('[data-delete]').unbind('click').on('click', function (e) {
                $this = $(this);
                $row = $this.parents('tr');

                var MODAL = $('#MyDeleteModal');

                var data = ($row.length && $this.parents('table').hasClass('dataTable')) ? $this.parents('table').DataTable().row($row).data() : new Object();
                e.preventDefault();

                $.ajax({
                    type: "GET",
                    data: data,
                    url: $this.data('deleteModalContentUrl'),
                    beforeSend: function () {
                        MODAL.find('.modal-title').html($this.data('deleteModalTitle'));
                        MODAL.find('.modal-body').find('form').html('Kérem várjon ...');
                        //MODAL.modal('toggle');
                    },
                    success: function (result) {
                        MODAL.find('button.btn-danger').data('url', $this.data('deleteSubmitUrl'));
                        MODAL.find('.modal-body').find('form').html(result);
                        MODAL.find('.modal-body').find('form').removeClass('hidden');
                    },
                    error: function (error) {
                    }
                });
            });
        }


        $(function () {
            var MODAL = $('#MyDeleteModal');

            MODAL.on('hidden.bs.modal', function (e) {
                $('#MyDeleteModal .modal-body form').children().remove();
            });

            $('#MyDeleteModal button.btn-danger').on('click', function () {
                $this = $(this);

                if (!$this.data('url') && typeof callbackDeleteModal === "function") {
                    callbackDeleteModal($this);
                } else {
                    var data = MODAL.find('form').serialize();

                    $.ajax({
                        type: "POST",
                        cache: false,
                        data: data,
                        url: $this.data('url'),
                        beforeSend: function () {
                            MODAL.find('.modal-error-div').addClass('hidden').html('');
                        },
                        success: function (result) {
                            if (result.trim()) {
                                if (result.substring(0, 5) == 'eval;') {
                                    eval(result.substring(5));
                                } else {
                                    MODAL.find('.modal-body').find('form').html(result);
                                    MODAL.animate({
                                        scrollTop: MODAL.offset().top
                                    }, 2000);
                                }
                            } else {
                                if ($this.data('redirectUrl')) {
                                    document.location = $this.data('redirectUrl');
                                } else {
                                    document.location.reload(true);
                                }
                            }
                        },
                        error: function (error) {
                            if (error.status == 422) {
                                // Validation error
                                var errorsHtml = '';

                                $.each(error.responseJSON, function (key, value) {
                                    errorsHtml += '<p>' + value[0] + '</p>';
                                });
                                errorsHtml += '';

                                MODAL.find('.modal-form-error').removeClass('hidden').html(errorsHtml);
                            } else if (error.status == 500) {
                                // Internal Server Error
                                MODAL.find('.modal-form-error').removeClass('hidden').html(error.responseJSON.error);
                            }
                        }
                    });
                }
            });

            deleteModalInit();
        });
    </script>
@endpush