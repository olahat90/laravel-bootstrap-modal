<?php

namespace Olahat90\LaravelBootstrapModal;

use Illuminate\Support\ServiceProvider;

class LaravelBootstrapModalServiceProvider extends ServiceProvider
{
    public function boot()
    {
        // $this->loadViewsFrom(__DIR__ . '/resources/shared/modals', 'laravelBootstrapModal');

        $this->publishes([
            __DIR__ . '/config/main.php' => config('laravel_bootstrap_modal.copyTo.config'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/resources/shared/modals' => config('laravel_bootstrap_modal.copyTo.view'),
        ], 'view');
    }

    public function register()
    {
        $this->app->bind('olahat90-laravelBootstrapModal', function () {
            return new LaravelBootstrapModal;
        });

        $this->mergeConfigFrom(
            __DIR__ . '/config/main.php', 'laravel_bootstrap_modal'
        );
    }
}