<?php

namespace Olahat90\LaravelBootstrapModal\Traits;

use Illuminate\Console\DetectsApplicationNamespace;
use Illuminate\Http\Request;

trait ModalTrait
{

    use DetectsApplicationNamespace;

    protected $config = [];
    protected $namespaces = [];

    private function _setConfig()
    {
        $this->config = config('laravel_bootstrap_modal');
        $this->namespaces = $this->config['namespaces'];
    }

    private function _setModel(Request $request)
    {
        $this->_setConfig();

        if ($this->namespaces['model'] == '') {
            $model = rtrim($this->getAppNamespace(), '\\') . '\\Models\\' . ucfirst(camel_case($request->route('model')));
        } else {
            $model = $this->namespaces['model'] . '\\' . ucfirst(camel_case($request->route('model')));
        }

        if (class_exists($model)) {
            if ($request->route('id') == 0 || $request->route('id') == 'new' || is_null($request->route('id')) || $request->route('id') == '') {
                // New
                $this->{$this->config['contentParamsName']}[ucfirst(camel_case($request->route('model'))) . 'Model'] = new $model();
            } else {
                // ID
                $this->{$this->config['contentParamsName']}[ucfirst(camel_case($request->route('model'))) . 'Model'] = $model::findOrFail($request->route('id'));
            }
        } else {
            $this->{$this->config['contentParamsName']}[ucfirst(camel_case($request->route('model'))) . 'Model'] = 'Not found';
        }
    }

    private function _createClassName()
    {
        $currentClass = get_class($this);

        $pos = strrpos($currentClass, '\\');
        if ($pos) {
            $className = substr($currentClass, $pos + 1);
        } else {
            $className = $pos;
        }
        $className = str_replace('Controller', '', $className);

        return $className;
    }

    private function _createMethodName(Request $request, $type)
    {
        $methodName = '_set' . ucfirst(camel_case($request->route('model'))) . ucfirst($type) . 'ModalContentParams';

        return $methodName;
    }

    private function _createEditorViewName(Request $request, $className, $type)
    {
        $currentClass = get_class($this);

        $appNamespace = rtrim($this->getAppNamespace(), '\\');
        $viewNamespace = str_replace($appNamespace . '\\Http\\Controllers\\', '', $currentClass);
        $viewNamespace = str_replace(ucfirst($className) . 'Controller', '', $viewNamespace);

        $view = str_replace('\\', '.', strtolower($viewNamespace)) . camel_case($className) . '/' . $type . 'Modals/' . camel_case($request->route('model'));

        return $view;
    }

    public function getEditorModalContent(Request $request)
    {
        $this->_setModel($request);

        // Name
        $className = $this->_createClassName();
        $methodName = $this->_createMethodName($request, 'editor');

        // Plus Attr
        if ($request->route('szemely')) {
            $this->{$this->config['contentParamsName']}['route']['szemely'] = $request->route('szemely');
        }

        // Method Exists
        if (method_exists($this, $methodName)) {
            $this->$methodName();
        }

        // View file
        $view = $this->_createEditorViewName($request, $className, 'editor');

        // Response
        return view($view, $this->{$this->config['contentParamsName']});
    }

    public function getDisplayModalContent(Request $request)
    {
        $this->_setModel($request);

        // Name
        $className = $this->_createClassName();
        $methodName = $this->_createMethodName($request, 'display');

        // Plus Attr
        if ($request->route('szemely')) {
            $this->{$this->config['contentParamsName']}['route']['szemely'] = $request->route('szemely');
        }

        // Method Exists
        if (method_exists($this, $methodName)) {
            $this->$methodName();
        }

        // View file
        $view = $this->_createEditorViewName($request, $className, 'display');

        // Response
        return view($view, $this->{$this->config['contentParamsName']} + ['kerelemShow' => true]);
    }

    public function getDeleteModalContent(Request $request)
    {
        $this->_setModel($request);

        // Name
        $className = $this->_createClassName();
        $methodName = $this->_createMethodName($request, 'delete');

        // Plus Attr
        if ($request->route('szemely')) {
            $this->{$this->config['contentParamsName']}['route']['szemely'] = $request->route('szemely');
        }

        // Method Exists
        if (method_exists($this, $methodName)) {
            $this->$methodName();
        }

        // View file
        $view = $this->_createEditorViewName($request, $className, 'delete');

        // Response
        return view($view, $this->{$this->config['contentParamsName']});
    }

    public function getConfirmModalContent(Request $request)
    {
        $this->_setModel($request);

        // Name
        $className = $this->_createClassName();
        $methodName = $this->_createMethodName($request, 'confirm');

        // Plus Attr
        if ($request->route('szemely')) {
            $this->{$this->config['contentParamsName']}['route']['szemely'] = $request->route('szemely');
        }

        // Method Exists
        if (method_exists($this, $methodName)) {
            $this->$methodName();
        }

        // View file
        $view = $this->_createEditorViewName($request, $className, 'confirm');

        // Response
        return view($view, $this->{$this->config['contentParamsName']});
    }

}