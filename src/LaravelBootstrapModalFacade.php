<?php

namespace Olahat90\LaravelBootstrapModal;

use Illuminate\Support\Facades\Facade;

class LaravelBootstrapModalFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'olahat90-laravelBootstrapModal';
    }
}